InCore Semiconductors
########################

Website Link `here <https://incoresemi.gitlab.io/website/>`_

Theme
-------
- Theme Name: BizPage
- Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
- Author: BootstrapMade.com
- Author URL: https://bootstrapmade.com
