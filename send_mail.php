<?php
// Get the form data
$name = $_POST['name'];
$phone = $_POST['Phone_Number'];
$email = filter_var($_POST['Email-id'], FILTER_SANITIZE_EMAIL);
$message = $_POST['Message'];

// Set up the email
$to = "info@incoresemi.com";
$subject = "New form submission from $name";
$body = "Name: $name\nPhone Number: $phone\nEmail: $email\nMessage:\n$message";
$headers = "From: $email\r\nReply-To: $email\r\n";

// Send the email
if (mail($to, $subject, $body, $headers)) {
  // Email sent successfully
  $response = array('status' => 'success', 'message' => 'Thank you for your message!');
} else {
  // Email failed to send
  $response = array('status' => 'error', 'message' => 'Something went wrong. Please try again later.');
}

// Send the response back to the client
header('Content-Type: application/json');
echo json_encode($response);
?>